package Skipark;

/**
 * Trieda Cas vypisuje na konzolu priebeh zapisu do suboru, priebeh je iba
 * informativny
 * 
 */
public class Cas extends Thread {
	protected int progress = 5;
	protected boolean done = false;

	public void run() {										//metoda s vyuzitim vl�kien, zatial �o prebieha zapis do suboru, vypisuje sa 
		System.out.println("Priebeh zapisu do suboru: ");	//'informativny' priebeh na konzolu
		while (progress != 100) {
			System.out.print(progress + "% ");
			progress += 5;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
		System.out.println(progress + "% ");
		done = true;
	}

	public boolean getDone() {
		return done;
	}

}