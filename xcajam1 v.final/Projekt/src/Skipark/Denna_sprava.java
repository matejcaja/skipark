package Skipark;

import java.io.*;

import javax.swing.JOptionPane;

import osoba.*;
import technika.*;

/**
 * Sluzi na vytvorenie suboru dennaSprava.txt, rozsiruje triedu Thread, obsahuje
 * metodu run()<br>
 * a pomocou yield() si vymiena beh s triedou Cas
 * 
 * @see Cas
 */
public class Denna_sprava extends Thread {
	File dennaSprava = new File("dennaSprava.txt");  
	private boolean existuje = false;
	private boolean vytvoreny = false;
	private int zisk;

	public Denna_sprava() {
		super("Denna sprava");
	}
											
	public void run() {											//metoda kontroluje existenciu suboru,
		try {													//a z hodnot, ktore boli pouzite v programe vytvara	
			FileWriter zapis = new FileWriter(dennaSprava);		//subor		
			PrintWriter pisem = new PrintWriter(zapis);				

			if (dennaSprava.isFile() == false) {
				dennaSprava.createNewFile();
				System.out.println("Vytvoril sa subor sprava.txt");
			}

			zisk = Ly�iar.spravaZarobok() - Skipark_okno.c_strata;
			pisem.println("Pocet zamestnancov: ");
			pisem.println("Obsluha vleku:\t " + Obsluha_vleku.spravaCounter());
			pisem.println("Zachranar:\t" + Z�chran�r.spravaCounter());
			pisem.println("Pocet vyuzitych zariadeni: ");
			pisem.println("Vlek typu SlowSnow: " + Pomaly_vlek.spravaCounter());
			pisem.println("Vlek typu SuperSnow: " + Rychly_vlek.spravaCounter()
					+ "\n");
			pisem.println("Celkove naklady: " + Skipark_okno.c_strata + " eur");
			pisem.println("Celkovy zarobok: " + Ly�iar.spravaZarobok() + " eur");
			pisem.println("Celkovy zisk :" + zisk + " eur\n");
			if (zisk > 0)
				pisem.println("Gratulujeme, stredisko dosiahlo zisk!");
			else
				pisem.println("Bohuzial, stredisko bolo predane zahranicnemu investorovi pre nizku ziskovost");
			vytvoreny = true;
			zapis.close();
			Thread.yield();
		}

		catch (IOException e) {												//v pripade chyby sa zobrazi okno
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage(),	//s vypism chyby
					"Chyba pri praci so suborom", JOptionPane.ERROR_MESSAGE);
		}
	}

	public boolean kontrolaVytvorenia() {	//metoda brzdi uzivatela, ak stlaci tlacidlo Zobraz Subor
		return vytvoreny;					//skor, ako bol vytvoreny
	}

	public boolean kontrolaSubor() {
		if (dennaSprava.exists() == true) {		//metoda kontroluje, �e dany subor existuje,
			return existuje = true;				//tym padom je povoleny vypis na obrazovku
		}
		return existuje;
	}

}
