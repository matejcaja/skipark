package Skipark;

/**
 * @version OOP Projekt, v3.0.0
 * @author Matej Caja<br>
 * <br>
 * 
 *         <b> Pouzite zdroje: </b>
 *         <ol>
 *         <li>http://www.javaswing.org/
 *         <li>http://docs.oracle.com/javase/tutorial/index.html
 *         <li>
 *         http://docs.oracle.com/javase/1.4.2/docs/api/overview-summary.html
 *         <li>Eckel Bruce, Thinking in Java 3rd edition
 *         <li>Herout Pavel, Ucebnice jazyka Java
 *         <li>Herout Pavel, Java graficke uzivatelske prostredi a cestina
 *         <li>Vranic Valentino, Objekty, Java a aspekty
 *         </ol>
 */

public class Skipark {
	/**
	 * Main metoda v triede Skipark zobrazuje GUI<br>
	 * 
	 * @see Skipark_okno
	 */
	public static void main(String[] args) {
		Skipark_okno GUI = new Skipark_okno();
		GUI.frame.setVisible(true);

	}

}
