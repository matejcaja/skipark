package Skipark;

import javax.swing.*;

import osoba.*;
import technika.*;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Trieda Skipark_okno je riadiaca trieda, vytvara a umoznuje funkcnost GUI
 * 
 * @see osoba
 * @see technika
 */
public class Skipark_okno extends JFrame {
	/* v nasledujucich riadkoch su zadefinovane vsetky premenne, ktore sa vyuzivaju v tejto triede */
	Osoba t;
	Technika tech;
	private int counter = 1;
	private int counter_porucha = 0;		
	private int pocet;
	private int pocet_fast = 0;
	private int suma = 200;
	private String vypis_suma;
	private double pridaj_koeficient = 2.0;
	static int c_strata = 0;
	String kapacita;
	int kap = 0;
	int counter_postupnost = 0;
	int counter_postupnost2 = 0;
	
	ArrayList<Object> pracovnici = new ArrayList<Object>();
	ArrayList<Object> technika = new ArrayList<Object>();	//definovanie jednotlivych ArrayListov
	ArrayList<Object> lyziari = new ArrayList<Object>();

	Obsluha_vleku vlekar = new Obsluha_vleku();
	Z�chran�r zachranar = new Z�chran�r();
	Ly�iar zjazdar = new Ly�iar();
	Pomaly_vlek slow = new Pomaly_vlek("SlowSnow");
	Rychly_vlek fast = new Rychly_vlek("SuperSnow");
	Denna_sprava zapisujem = new Denna_sprava();
	Cas progress = new Cas();

	final JFrame frame = new JFrame("SkiPark centrum");
	private JButton dalsi = new JButton("=>");
	private JButton tlacidlo = new JButton("OK");
	private JButton tlacidlo2 = new JButton("Pridaj vlekara");
	private JButton tlacidlo3 = new JButton("Oprav vlek");
	private JButton tlacidlo4 = new JButton("Vytvor subor");		//definovanie vsetkych tlacidiel
	private JButton tlacidlo6 = new JButton("OK");
	private JButton tlacidlo5 = new JButton("Pridaj zachranara");
	private JButton tlacidlo7 = new JButton("Zobraz subor");
	private JButton tlacidlo8 = new JButton("OK");
	private JButton tlacidlo9 = new JButton("OK");
	private JButton tlacidlo10 = new JButton("Otvorit stredisko");
	private JButton tlacidlo11 = new JButton("Aktualny stav strediska");
	private JButton tlacidlo12 = new JButton("Pridaj vlek");
	private JButton tlacidlo13 = new JButton("Pridaj lyziarov");
	private JLabel Stav_konta = new JLabel("Stav konta: 200 eur");
	private JLabel Stav_konta2 = new JLabel("Stav konta: 200 eur");
	private JLabel koniec = new JLabel("Koniec simulacie");
	private JTextArea vypis = new JTextArea(20, 45);
	private JTextArea vypis_subor = new JTextArea(20, 45);
	private JTextArea vypis_pracovnici = new JTextArea(
			"Obsluha velku: 10 eur\nZachranar: 20 eur\nPomaly vlek: 30 eur\nRychly vlek: 40 eur\n\n",
			20, 45);
	private JScrollPane scrollPane = new JScrollPane(vypis,			//definovanie vypisovych okien
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	private JScrollPane scrollPane_subor = new JScrollPane(vypis_subor,
			JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	private JScrollPane scrollPane_pracovnici = new JScrollPane(
			vypis_pracovnici, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
			JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	private JLabel vloz1 = new JLabel("Pocet vlekarov:");
	private JLabel vloz2 = new JLabel("Pracovne skusenosti[1/0]:");
	private JLabel vloz3 = new JLabel("Pocet zachranarov:");
	private JLabel vloz4 = new JLabel("Pracovne skusenosti[1/0]:");
	private JLabel vloz5 = new JLabel("Pocet vlekov typu SlowSnow:");
	private JLabel vloz6 = new JLabel("Kapacita[10/20]:");
	private JLabel vloz7 = new JLabel("Pocet vlekov typu SuperSnow:");
	private JLabel vloz8 = new JLabel("Kapacita[10/20]:");
	private JTextField text1 = new JTextField(10);
	private JTextField text2 = new JTextField(8);
	private JTextField text3 = new JTextField(8);		//definovanie okien na zadavanie hondot od uzivatela
	private JTextField text4 = new JTextField(8);
	private JTextField text5 = new JTextField(8);
	private JTextField text6 = new JTextField(8);
	private JTextField text7 = new JTextField(8);
	private JTextField text8 = new JTextField(8);
	JPanel cards = new JPanel(new CardLayout());
	JPanel card1 = new JPanel();
	JPanel card2 = new JPanel();				//definovanie jednotlivych kariet
	JPanel card3 = new JPanel();
	JPanel ovladac = new JPanel();
	final String NEXT = "NEXT";

	class ControlActionListenter implements ActionListener {		
		public void actionPerformed(ActionEvent e) {			//metoda umoznuje po kliknuti na tlacidlo 
			CardLayout cl = (CardLayout) (cards.getLayout());	//prechod na dalsiu kartu
			String cmd = e.getActionCommand();
			if (cmd.equals(NEXT)) {
				cl.next(cards);

			}

		}
	}

	ControlActionListenter cal = new ControlActionListenter();

	public Skipark_okno() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	//zadefinovanie velkosti, typu layoutu
		frame.setTitle("Vitajte v SkiPark centre!");			//a defaultneho zatvarania okna
		setLayout(new FlowLayout());
		frame.setSize(580, 600);

		//premenne operujuce s Obsluha vleku
		card1.add(vloz1);
		card1.add(text1);
		card1.add(vloz2);
		card1.add(text2);
		card1.add(tlacidlo);
		//premenne operujuce so Zachranar
		card1.add(vloz3);
		card1.add(text3);
		card1.add(vloz4);
		card1.add(text4);									
		card1.add(tlacidlo6);
		//premenne operujuce s pomaly vlek				
		card1.add(vloz5);
		card1.add(text5);							//kazdy prikaz je prideleny na svoju vlastnu kartu
		card1.add(vloz6);
		card1.add(text6);
		card1.add(tlacidlo8);
		//premenne operujuce s rychly vlek
		card1.add(vloz7);
		card1.add(text7);
		card1.add(vloz8);
		card1.add(text8);
		card1.add(tlacidlo9);
		//premenne operujuce s vypis a stav
		card1.add(scrollPane_pracovnici);
		card1.add(Stav_konta);

		//premenne operujuce s praca so strediskom
		card2.add(tlacidlo10);
		card2.add(tlacidlo11);
		card2.add(tlacidlo3);
		tlacidlo11.setEnabled(false);
		card2.add(scrollPane);
		card2.add(tlacidlo2);
		card2.add(tlacidlo5);
		card2.add(tlacidlo12);
		card2.add(tlacidlo13);
		card2.add(Stav_konta2);

		//premenne operujuce s praca so suborom
		card3.add(tlacidlo4);
		card3.add(tlacidlo7);
		card3.add(scrollPane_subor);
		card3.add(koniec);

		cards.add(card1);
		cards.add(card2);
		cards.add(card3);

		dalsi.setActionCommand(NEXT);
		dalsi.addActionListener(cal);

		ovladac.add(dalsi);								//nastavenie tlacidla '=>', tlacidlo je vycentrovane a nastavene
		Container pane = frame.getContentPane();		//pomocou BorderLayout na spodok frame-u
		pane.add(cards, BorderLayout.CENTER);
		pane.add(ovladac, BorderLayout.PAGE_END);

		dalsi.addActionListener(new ActionListener() {		//ActionListener tlacidla '=>' zabezpeceuje, ze
			public void actionPerformed(ActionEvent e) {	//po prejdeni na poslednu kartu sa stane 
				counter++;									//nefunckne resp. disabled
				if (counter == 3)
					dalsi.setEnabled(false);
				;
			}
		});
		/* v nalsedujucich riadkoch su vytvorene udalosti pre jednotlive tlacidla, metody action_Performed(e)
		 * sa nachadzaju za tymyto udalostami
		 */
		tlacidlo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nacitajOV_actionPerformed(e);
			}
		});

		tlacidlo2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pridajOV_actionPerformed(e);
			}
		});

		tlacidlo3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				oprava_actionPerformed(e);
			}
		});

		tlacidlo4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				subor_actionPerofrmed(e);
			}
		});

		tlacidlo5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pridajZCH_actionPerformed(e);
			}
		});

		tlacidlo6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nacitajZCH_actionPerformed(e);
			}
		});

		tlacidlo7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zobraz_subor_actionPerformed(e);
			}
		});

		tlacidlo8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nacitaj_slowsnow_actionPerformed(e);
			}
		});

		tlacidlo9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nacitaj_supersnow_actionPerformed(e);
			}
		});

		tlacidlo10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				spustit_actionPerformed(e);
			}
		});

		tlacidlo11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aktstav_actionPerformed(e);
			}
		});

		tlacidlo12.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pridajPV_actionPerformed(e);
			}
		});

		tlacidlo13.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pridajLYZ_actionPerformed(e);
			}
		});
	}

	private void pridajLYZ_actionPerformed(ActionEvent e) {
		if (pracovnici.isEmpty() == true) {
			vypis.append("Stredisko bolo ludoprazdne, tak odisli\n");
		} else {
			zjazdar.setNarodnost(false);
			zjazdar.getNarodnost();
			zjazdar.setVymena("1");						//tlacidlo pridava najprv domacich lyziarov
			zjazdar.getVymena();						//nastavia sa jednotlive metody, na zaklade ktorych
			pocet = zjazdar.pocetLyziarov();			//sa meni Stav konta, pocet lyziarov v stredisku
			for (int i = 0; i < pocet; i++) {			//a v cykle ich prida do ArrayListu
				suma += (zjazdar.Zarobok());
				lyziari.add(zjazdar);
			}
			vypis.append("Do strediska prislo " + pocet + " lyziarov "
					+ zjazdar.zaradLyziara() + "\n");
			zjazdar.setNarodnost(true);					//zmena metod na zahranicnych lyziarov
			zjazdar.getNarodnost();						
			zjazdar.setVymena("1");
			zjazdar.getVymena();
			pocet = zjazdar.pocetLyziarov();
			for (int i = 0; i < pocet; i++) {			//v cykle sa pridaju do ArrayListu
				suma += (zjazdar.Zarobok());
				lyziari.add(zjazdar);
			}
			vypis_suma = String.valueOf(suma);
			Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
			vypis.append("Do strediska prislo " + pocet + " lyziarov "
					+ zjazdar.zaradLyziara() + "\n");
		}

	}

	private void aktstav_actionPerformed(ActionEvent e) {
		counter_porucha = 0;
		for (int i = 0; i < technika.size(); i++) {	//pri stlaceni sa zobrazi stav strediska, zistuje sa
			tech = (Technika) technika.get(i);		//ci nieje kazivost triedy Rychly_vlek nastavena na
			if (tech.kazivostVleku() == 1) {		//1, pricom sa vyuziva polymorfizmus
				kap -= 20;							//vyuziva sa premenna tech typu Technika
				counter_porucha++;
			}
		}
		/* vypisy do JTextArea v oboch pripadoch, ak je alebo nieje porucha vlekov */
		vypis.append("\n" + counter_porucha + " polofunkcnych vlekov\n");
		if (counter_porucha != 0)
			vypis.append("Kapacita vlekov klesla na " + kap + "\n");
																		//nespokojny lyziari odchadzaju, znizuje sa
		if (kap < lyziari.size()) {										//kapacita vlekov a aj Stav konta
			vypis.append("Vleky su preplnene, lyziari su nespokojni a niektori odchadzaju[\n");
			for (int i = 0; i < 5; i++) {
				lyziari.remove(i);
				suma -= (zjazdar.Zarobok(3));
			}
		} else if (lyziari.isEmpty() == false) {
			vypis.append("Lyziari sa spokojne venuju svojmu sportu\n");	//pripad ked su vsetci spokojni
		}
		vypis_suma = String.valueOf(suma);
		Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
		vypis.append("Aktualna kapacita vlekov " + kap + "\n");
		vypis.append("Aktualny pocet lyziarov " + lyziari.size() + "\n\n");
	}

	private void pridajPV_actionPerformed(ActionEvent e) {
		if (pracovnici.isEmpty() == true)				//osetrenie v pripade, ze uzivatel nezadal
			vypis.append("Vlek nema kto spustit\n");	//ziadneho pracovnika 
		else {
			technika.add(fast);
			fast.setKazivost(1);
			suma -= pridaj_koeficient * 10;			//tlacidlo prida vlek, nastavi sa v triede Rychly_vlek 
			c_strata += pridaj_koeficient * 10;		//kazivost na 1 a je nutne opravit vsetky vleky
			vypis_suma = String.valueOf(suma);		//tohto typu
			fast.pocetSuperSnow();
			vypis.append("Spustil sa dalsi vlek typu SuperSnow\n");
			kap += 20;
			pocet_fast += 1;
			if (counter_postupnost2 % 2 == 1) {
				fast.setKazivost(1);				//nastavenie kazivosti 'na preskacku'
			} else {
				fast.setKazivost(0);
			}
			counter_postupnost2++;
		}
	}

	private void spustit_actionPerformed(ActionEvent e) {
		vypis.append("Stredisko je pristupne verejnosti!\n");	//tlacidlo na spristupnenie strediska
		tlacidlo11.setEnabled(true);
		tlacidlo10.setEnabled(false);

	}

	private void oprava_actionPerformed(ActionEvent e) {
		/* uplatnenie polymorfizmu */
		if (pracovnici.isEmpty() == true) {
			vypis.append("Nedostatok presonalu\n");		//osetrenia ak nieje nastaveny ziaden vlek alebo personal
		} else if (technika.isEmpty() == true)
			vypis.append("Nebol spusteny ziaden vlek");
		else {
			if (counter_postupnost % 2 == 0) {
				lyziari.add(zachranar);				//v prvom pokuse sa prida zachranar do ArrayListu
				pracovnici.remove(zachranar);		//lyziarov a pomocou premennej t sa zistuje, ci sa dany vlek opravil
				for (int i = lyziari.size() - 8; i < lyziari.size(); i++) {
					t = (Osoba) lyziari.get(i);
					vypis.append(t.vieOpravovat());
				}
				counter_postupnost++;
			} else if (counter_postupnost % 2 == 1) {
				lyziari.add(vlekar);					//v druhom pokuse sa prida zachranar, ktory dokaze vlek opravit
				pracovnici.remove(vlekar);				
				for (int i = lyziari.size() - 8; i < lyziari.size(); i++) {
					t = (Osoba) lyziari.get(i);
					vypis.append(t.vieOpravovat());
				}
				counter_postupnost++;
			}
			if (lyziari.contains(vlekar)) {		//vlekar a zachranar sa vymazu z daneho ArrayListu a nastavi� sa
				lyziari.remove(vlekar);			//metoda setKazivost() na nulu
				if (lyziari.contains(zachranar))
					lyziari.remove(zachranar);
				fast.setKazivost(0);
				kap += 20 * pocet_fast;
			}
		}
		/* 'lyziari.size() - 8' je preto, aby sa nevypisoval dlhy rad metody vieOpravovat() pre kazdeho
		 * zvlast, zobrazi sa iba cast, ktora je najpodstatnejsia 
		 */
	}

	public void pridajZCH_actionPerformed(ActionEvent e) {
		vypis.append("POZOR! pracovnik musel neplanovane nastupit do prace,jeho koeficient vyplaty je preto 2\n");
		pracovnici.add(zachranar);
		suma -= pridaj_koeficient * 10;				//v pripade nedostatku personalu tla�idlo prid� zachranarov
		c_strata += pridaj_koeficient * 10;			//do ArrayListu a zaroven sa nastavuju hodnoty pre vypis 
		vypis_suma = String.valueOf(suma);			//do suboru a Stav konta
		zachranar.pocetZachranarov();
		Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");

		vypis.append("Do prace nastupil dalsi zachranar\n");
	}

	private void pridajOV_actionPerformed(ActionEvent e) {
		vypis.append("POZOR! pracovnik musel neplanovane nastupit do prace,jeho koeficient vyplaty je preto 2\n");
		pracovnici.add(vlekar);
		suma -= pridaj_koeficient * 20;								//v pripade nedostatku personalu, alebo nedostatku
		c_strata += pridaj_koeficient * 20;							//vlekarov tla�idlo prid� vlekara do ArrayListu
		vypis_suma = String.valueOf(suma);							//zaroven sa nastavuju hodnoty pre vypis do suboru
		vlekar.pocetVlekarov();										//a Stav konta
		Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");

		vypis.append("Do prace nastupil dalsi vlekar\n");

	}

	public void nacitaj_supersnow_actionPerformed(ActionEvent e)
			throws NumberFormatException {
		try {
			int pocet;
			double ziskaj_koeficient;
			double naklady;

			pocet = Integer.valueOf(text7.getText()).intValue();
			pocet_fast += pocet;
			fast.setKapacita(text8.getText());						//po stla�eni tla�idla sa zisti dan� po�et
			fast.getKapacita();										//vlekov, zadany od u��vate�a a nastavia sa
			fast.koeficient();										//jednotlive metody
			fast.setKazivost(0);
			ziskaj_koeficient = fast.getKoeficient();
			naklady = fast.Naklady(40);

			for (int i = 0; i < pocet; i++) {
				fast.pocetSuperSnow();							//v cykle sa zapise dany pocet vlekov do
				suma -= naklady;								//ArrayListu
				c_strata += naklady;
				technika.add(fast);
				kap += Integer.valueOf(text8.getText()).intValue();
			}

			vypis_suma = String.valueOf(suma);
			
			/* nasledujuce riadky zobrazuju vypisy do JTextArea */
			if (suma < 0) {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				vypis_pracovnici.append("\nPOZOR! SkiPark je v minuse\n");
			} else {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
			}

			vypis_pracovnici.append("Spustilo sa " + text7.getText()
					+ " vlekov typu SuperSnow\n");
			vypis_pracovnici
					.append("Koeficient nakladov pri danej kapacite je "
							+ ziskaj_koeficient + "\n");
			vypis_pracovnici.append("Naklady na jeden vlek su " + naklady
					+ " eur\n\n");
		} catch (NumberFormatException e2) {										//osetrenie zadavania od uzivatela
			JOptionPane.showMessageDialog(null, e2.getLocalizedMessage(),			//ak nezada cislo,vypise sa varovne okno
					"Chyba pri zadavani parametrov", JOptionPane.ERROR_MESSAGE);
		}

	}

	public void nacitaj_slowsnow_actionPerformed(ActionEvent e)
			throws NumberFormatException {
		try {
			int pocet;
			double ziskaj_koeficient;
			double naklady;

			pocet = Integer.valueOf(text5.getText()).intValue();
			slow.setKapacita(text6.getText());						//po stla�eni tla�idla sa zisti dan� po�et
			slow.getKapacita();										//vlekov, zadany od u��vate�a a nastavia sa
			slow.koeficient();										//jednotliv� metody
			ziskaj_koeficient = slow.getKoeficient();
			naklady = slow.Naklady(30);

			for (int i = 0; i < pocet; i++) {
				slow.pocetSlowSnow();
				suma -= naklady;
				c_strata += naklady;
				technika.add(slow);
				kap += Integer.valueOf(text6.getText()).intValue();
			}

			vypis_suma = String.valueOf(suma);
			
			/* nasledujuce riadky zobrazuju vypisy do JTextArea */
			if (suma < 0) {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				vypis_pracovnici.append("\nPOZOR! SkiPark je v minuse\n");
			} else {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
			}

			vypis_pracovnici.append("Spustilo sa " + text5.getText()
					+ " vlekov typu SlowSnow\n");
			vypis_pracovnici
					.append("Koeficient nakladov pri danej kapacite je "
							+ ziskaj_koeficient + "\n");
			vypis_pracovnici.append("Naklady na jeden vlek su " + naklady
					+ " eur\n\n");
		} catch (NumberFormatException e2) {										//osetrenie zadavania od uzivatela
			JOptionPane.showMessageDialog(null, e2.getLocalizedMessage(),			//ak nezada cislo,vypise sa varovne okno
					"Chyba pri zadavani parametrov", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void nacitajZCH_actionPerformed(ActionEvent e)
			throws NumberFormatException {
		try {
			double ziskaj_skusenosti;
			double vyplata;
			int pocet;

			pocet = Integer.valueOf(text3.getText()).intValue();
																	//po stla�en� tla�idla sa zapisuje dany pocet zachranarov
			zachranar.setSkusenosti(text4.getText());				//zadany uzivatelom a nastavia sa jednotliv� metody
			zachranar.getSkusenosti();
			zachranar.koeficient();
			ziskaj_skusenosti = zachranar.getKoeficient();
			vyplata = zachranar.Vyplata(20);

			for (int i = 0; i < pocet; i++) {
				zachranar.pocetZachranarov();				//v cykle sa dany pocet vlekarov zapise do ArrayListu
				suma -= vyplata;							//a ziskavaju sa hodnoty
				c_strata += vyplata;
				pracovnici.add(zachranar);
			}
			vypis_suma = String.valueOf(suma);
			
			/* nasledujuce riadky zobrazuju vypisy do JTextArea */
			if (suma < 0) {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				vypis_pracovnici.append("\nPOZOR! SkiPark je v minuse\n");
			} else {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
			}
			vypis_pracovnici.append("Do prace nastupilo " + text3.getText()
					+ " Zachranarov\n");
			vypis_pracovnici
					.append("Koeficient vyplaty pri danych skusenostiach je "
							+ ziskaj_skusenosti + "\n");
			vypis_pracovnici.append("Naklady na osobu su " + vyplata + "\n\n");
		} catch (NumberFormatException e2) {										//osetrenie zadavania od uzivatela
			JOptionPane.showMessageDialog(null, e2.getLocalizedMessage(),			//ak nezada cislo,vypise sa varovne okno
					"Chyba pri zadavani parametrov", JOptionPane.ERROR_MESSAGE);

		}

	}

	private void nacitajOV_actionPerformed(ActionEvent e)
			throws NumberFormatException {
		try {
			double ziskaj_skusenosti;
			double vyplata;
			int pocet;

			pocet = Integer.valueOf(text1.getText()).intValue();

			vlekar.setSkusenosti(text2.getText());			//po stla�en� tla�idla sa zapisuje dany pocet vlekarov
			vlekar.getSkusenosti();							//zadany uzivatelom a nastavia sa jednotliv� metody
			vlekar.koeficient();
			ziskaj_skusenosti = vlekar.getKoeficient();
			vyplata = vlekar.Vyplata(10);

			for (int i = 0; i < pocet; i++) {
				vlekar.pocetVlekarov();				//v cykle sa dany pocet vlekarov zapise do ArrayListu
				suma -= vyplata;					//a ziskavaju sa hodnoty
				c_strata += vyplata;
				pracovnici.add(vlekar);
			}
			vypis_suma = String.valueOf(suma);
			
			/* nasledujuce riadky zobrazuju vypisy do JTextArea */
			if (suma < 0) {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur");
				vypis_pracovnici.append("POZOR! SkiPark je v minuse\n");
			} else {
				Stav_konta.setText("Stav konta " + vypis_suma + " eur\n");
				Stav_konta2.setText("Stav konta " + vypis_suma + " eur\n");
			}															

			vypis_pracovnici.append("Do prace nastupilo " + text1.getText()
					+ " pracovnikov oblsuhy vleku\n");
			vypis_pracovnici
					.append("Koeficient vyplaty pri danych skusenostiach je "
							+ ziskaj_skusenosti + "\n");
			vypis_pracovnici.append("Naklady na osobu su " + vyplata
					+ " eur\n\n");
		}

		catch (NumberFormatException e2) {									//osetrenie zadavania od uzivatela 
			JOptionPane.showMessageDialog(null, e2.getLocalizedMessage(),	//ak nezada cislo,vypise sa varovne okno
					"Chyba pri zadavani parametrov", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void zobraz_subor_actionPerformed(ActionEvent e) {
		if (zapisujem.kontrolaVytvorenia() == false) {				//po stla�en� pr�slu�n�ho tla�idla sa kontroluje
			vypis_subor.append("Subor este nebol vytvoreny\n");		//subor prislusnymi metodami
		} else if (zapisujem.kontrolaSubor() == true) {
			try {														
				BufferedReader buffer = new BufferedReader(new FileReader(
						"dennaSprava.txt"));
				String riadok;
				while ((riadok = buffer.readLine()) != null) {		//cita sa subor po riadkoch a vypisuje 
					vypis_subor.append(riadok);						//sa na obrazovku 
					vypis_subor.append("\n");
				}

				buffer.close();
			} catch (IOException e1) {			//v pripade vstupno-vystupnej chybe vyskoci varovne okno
				JOptionPane
						.showMessageDialog(null, e1.getLocalizedMessage(),
								"Chyba pri praci so suborom",
								JOptionPane.ERROR_MESSAGE);

			}
		}
	}

	public void subor_actionPerofrmed(ActionEvent e) {
		zapisujem.setDaemon(true);			//po stla�en� sa spusti zapis do suboru
		zapisujem.start();					//a zaroven bude na konzole vypisany informativny progres
		progress.start();

		zapisujem.kontrolaVytvorenia();		//vyuziva sa metoda z triedyDenna_sprava, 
											//kontroluje sa vytvorenie suboru
		vypis_subor.append("Subor bude k nahliadnutiu\n");
	}

}
