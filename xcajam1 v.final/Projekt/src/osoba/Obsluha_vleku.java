package osoba;

/**
 * Trieda specifikuje vlekarov v stredisku, vyuziva interface Transakcie a
 * metody zdedene<br>
 * z nadtriedy, uzivatel nastavuje pocet a skusenosti vlekarov v stredisku,
 * metoda spravaCounter<br>
 * posiela udaje triede Denna_sprava.
 */

public class Obsluha_vleku extends Osoba implements Transakcie {
	private static int counter = 0;
	protected double pracovne_skusenosti;
	protected String skusenosti;

	public String vieOpravovat() {		//uplatnenie polymorfizmu
		String s = "Obsluha vleku vzala do ruky kladivo a pracuje na oprave vlekov\n";
		return s;
	}

	public static int spravaCounter() {		//metoda odosiela hodnotu do triedy Denna_sprava
		return counter;
	}

	public double getKoeficient() {		//metoda zdedena z triedy Osoba, ziskava koeficient,  
		return koeficient;				//od ktor�ho zavisia naklady na jedneho vlekara
	}

	public double Vyplata(double vyska_vyplaty) {		//metoda vyuziva koeficient a nasobi ho s vyplatou 
		return vyska_vyplaty * koeficient;				//prisluchajucou vlekarom(10 eur)
	}

	public int pocetVlekarov() {			//metoda zistuje, kolko vlekarov bolo vyuzitych v stredisku
		counter++;							//pocet je zobrazeny vo vyslednom subore
		return counter;
	}

	public double Vyplata() {
		return 0;
	}

	@Override
	public String vieOsetrovat() {
		return null;
	}

	@Override
	public int Zarobok() {
		return 0;
	}

}
