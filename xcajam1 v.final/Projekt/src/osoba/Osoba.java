package osoba;

/**
 * Abstraktna trieda, od ktorej dedia zvysne dve triedy z balika osoba,<br>
 * trieda obsahuje metodu koeficient(), ktora nastavuje vyplaty jednotlivych
 * pracovnikov<br>
 * na zaklade ich pracovnych skusenosti, obsahuje abstraktnu metodu
 * vieOpravovat()<br>
 * a vieOsetrovat()(zatial nevyuzita) - uplatnenie polymorfizmu
 */
public abstract class Osoba {
	protected double koeficient = 0;
	private double hodnota1;
	
	/* vsetky triedy zdedene od triedy Osoba maju implicitny konstruktor */
	
	public double setSkusenosti(String pracovne_skusenosti) {		//metoda nastavuje pracovne skusenosti, na zaklade
		hodnota1 = Double.valueOf(pracovne_skusenosti).intValue();	//ktorych sa zistuju naklady na jednu osobu
		return hodnota1;
	}

	public double getSkusenosti() {		//getter na hodnota1, ktora obsahuje hodnotu 1/0 skusenosti
		return hodnota1;
	}

	public double koeficient() {		//metoda nastavuje koeficient vyplaty
		if (hodnota1 == 0) {
			return koeficient = 0.5;
		} else if (hodnota1 == 1) {
			return koeficient = 1;
		}
		return koeficient;

	}
	
	//metody na uplatnenie polymorfizmu
	public abstract String vieOsetrovat();

	public abstract String vieOpravovat();

}
