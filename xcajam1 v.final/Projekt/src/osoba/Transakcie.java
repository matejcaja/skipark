package osoba;

/**
 * Interface ma zadefinovane metody, ktore sa staraju o vyplatu a zarobok v
 * stredisku
 * 
 * @see Obsluha_vleku
 * @see Z�chran�r
 * @see Ly�iar
 */
public interface Transakcie {
	
	public double Vyplata();	

	public int Zarobok();

}
