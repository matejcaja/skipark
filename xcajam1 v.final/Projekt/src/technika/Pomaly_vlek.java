package technika;

/**
 * Trieda Pomaly_vlek obsahuje metody na specifikovanie a nastavenie
 * koeficientov,<br>
 * ktore sa menia priamo v programe na zaklade uzivatela
 * 
 */
public class Pomaly_vlek extends Technika {
	private int kazivost;
	private static int counter = 0;

	public Pomaly_vlek(String znacka) {
		super(znacka);
	}

	public int setKazivost(int kazivost) {		//setter na nastavenie kazivosti vleku
		return this.kazivost = kazivost;
	}

	public int getKazivost() {	
		return kazivost;
	}

	public int pocetSlowSnow() {		//metoda zistuje pocet vyuzivanych vlekov
		counter++;
		return counter;
	}

	public static int spravaCounter() {		
		return counter;
	}

	public double getKoeficient() {
		return koeficient;
	}

	public double Naklady(double vyska_nakladov) {		//prenasobenie nakladov koeficientom(jeho velkost zavisi od kapacity vleku)
		counter++;
		return vyska_nakladov * koeficient;
	}

	public int kazivostVleku() {		//uplatnenie polymorfizmu
		int k = getKazivost();
		return k;
	}
}
