package technika;

/**
 * Trieda Rychly_vlek obsahuje metody na specifikovanie a nastavenie
 * koeficientov,<br>
 * ktore sa menia priamo v programe na zaklade uzivatela
 * 
 */
public class Rychly_vlek extends Technika {
	private int kazivost;
	private static int counter = 0;

	public Rychly_vlek(String znacka) {
		super(znacka);
	}

	public int getKazivost() {
		return kazivost;
	}

	public int setKazivost(int kazivost) {		//setter na nastavenie kazivosti vleku
		return this.kazivost = kazivost;
	}

	public static int spravaCounter() {
		return counter;
	}

	public double getKoeficient() {
		return koeficient;
	}

	public int pocetSuperSnow() {		//metoda zistuje pocet vyuzivanych vlekov
		counter++;
		return counter;
	}

	public double Naklady(double vyska_nakladov) {
		return vyska_nakladov * koeficient;
	}
	
	public int kazivostVleku() {		//uplatnenie polymorfizmu
		int k = getKazivost();
		return k;
	}

}
