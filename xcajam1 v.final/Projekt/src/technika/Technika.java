package technika;

/**
 * Abstraktna trieda, od ktorej dedia zvysne dve triedy z balika technika,<br>
 * trieda obsahuje metody, ktore blizsie specifikuju podtriedy, obsahuje<br>
 * abstraktnu metodu kazivostVleku() - uplatnenie polymorfizmu
 */
public abstract class Technika {
	protected String znacka;
	private double hodnota2;
	protected double koeficient;
	
	/* vsetky triedy zdedene od triedy Technika maju explicitný konstruktor: */
	public Technika(String znacka) {		
		this.znacka = znacka;
	}
			
	public double setKapacita(String kapacita) {		//setter nastavuje kapacitu vleku
		hodnota2 = Integer.valueOf(kapacita).intValue();
		return hodnota2;
	}

	public double getKapacita() {		//getter ziskava hodnotu kapacity
		return hodnota2;
	}

	public double koeficient() {		//koeficient na zaklade hodnoty kapacity nastavi koeficient
		if (hodnota2 == 10) {
			return koeficient = 1.5;
		} else if (hodnota2 == 20) {
			return koeficient = 2;
		}
		return koeficient;

	}

	public abstract int kazivostVleku();	//metoda na uplatnenie polymorfizmu
}
